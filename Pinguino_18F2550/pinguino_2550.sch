EESchema Schematic File Version 2
LIBS:pinguino_2550-rescue
LIBS:24c1024
LIBS:74xgxx
LIBS:74xx
LIBS:74xx-us
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:Atmega8-16PI
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cap-master
LIBS:_cenditel
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:_conn_rf
LIBS:contrib
LIBS:_crydom
LIBS:_cui
LIBS:cypress
LIBS:DB-1
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:_diodes
LIBS:dips-s
LIBS:display
LIBS:double_zenner
LIBS:ds1307
LIBS:dsp
LIBS:ESD_Protection
LIBS:_fairchild
LIBS:_fci
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:_id-innovations
LIBS:_idt
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:Lattice
LIBS:led_array_4
LIBS:led_array_8
LIBS:linear
LIBS:logo
LIBS:logo_jc
LIBS:_lumex
LIBS:maxim
LIBS:_maxim
LIBS:memory
LIBS:microchip
LIBS:microchip1
LIBS:_microchip
LIBS:microchip_dspic33dsc
LIBS:_microchip_icsp
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:moto
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:_national
LIBS:ndy24xx
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:_onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:_rohm
LIBS:rs-24xx
LIBS:sensors
LIBS:sht15_mod
LIBS:silabs
LIBS:_silabs
LIBS:siliconi
LIBS:sma
LIBS:SparkFun
LIBS:special
LIBS:_special
LIBS:_st
LIBS:_std
LIBS:stm8
LIBS:stm32
LIBS:_st_poe
LIBS:_st_rf
LIBS:supertex
LIBS:switches
LIBS:sw_push_4
LIBS:Symbols_ICs-Diskrete_RevA
LIBS:SymbolsSimilarEN60617+oldDIN617
LIBS:Symbols_Transformer-Discrete
LIBS:temt6000
LIBS:texas
LIBS:tlp3542
LIBS:_totalphase
LIBS:transf
LIBS:transistors
LIBS:ttl_ieee
LIBS:_unknown
LIBS:valves
LIBS:video
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xxx
LIBS:Zilog
LIBS:pinguino_2550-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "Pingüino PIC18F2550"
Date "2016-06-20"
Rev "V.0"
Comp "CENDITEL - UPTM"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "ING. Josemiguel Canelones"
$EndDescr
$Comp
L PIC18F2550-RESCUE-pinguino_2550 PIC1
U 1 1 5767EFDD
P 4700 3000
F 0 "PIC1" H 4700 2000 70  0000 C CNN
F 1 "PIC18F2550" H 4700 1800 70  0000 C CNN
F 2 "Housings_DIP:DIP-28_W7.62mm_LongPads" H 4700 3000 60  0001 C CNN
F 3 "" H 4700 3000 60  0000 C CNN
	1    4700 3000
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-pinguino_2550 R2
U 1 1 5767F032
P 2500 1350
F 0 "R2" V 2580 1350 50  0000 C CNN
F 1 "10k" V 2500 1350 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 2430 1350 50  0001 C CNN
F 3 "" H 2500 1350 50  0000 C CNN
	1    2500 1350
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-pinguino_2550 R1
U 1 1 5767F0BB
P 2100 2000
F 0 "R1" V 2180 2000 50  0000 C CNN
F 1 "470" V 2100 2000 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 2030 2000 50  0001 C CNN
F 3 "" H 2100 2000 50  0000 C CNN
	1    2100 2000
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-pinguino_2550 R3
U 1 1 5767F14E
P 6650 1700
F 0 "R3" V 6730 1700 50  0000 C CNN
F 1 "470" V 6650 1700 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 6580 1700 50  0001 C CNN
F 3 "" H 6650 1700 50  0000 C CNN
	1    6650 1700
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-pinguino_2550 C3
U 1 1 5767F1EF
P 3250 3650
F 0 "C3" H 3275 3750 50  0000 L CNN
F 1 "220nF" H 3275 3550 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 3288 3500 50  0001 C CNN
F 3 "" H 3250 3650 50  0000 C CNN
	1    3250 3650
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-pinguino_2550 C4
U 1 1 5767F2C3
P 7850 3700
F 0 "C4" H 7875 3800 50  0000 L CNN
F 1 "100nf" H 7875 3600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 7888 3550 50  0001 C CNN
F 3 "" H 7850 3700 50  0000 C CNN
	1    7850 3700
	-1   0    0    1   
$EndComp
$Comp
L C-RESCUE-pinguino_2550 C1
U 1 1 5767F356
P 2350 4300
F 0 "C1" H 2375 4400 50  0000 L CNN
F 1 "22pF" H 2375 4200 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2388 4150 50  0001 C CNN
F 3 "" H 2350 4300 50  0000 C CNN
	1    2350 4300
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-pinguino_2550 C2
U 1 1 5767F39A
P 2700 4500
F 0 "C2" H 2725 4600 50  0000 L CNN
F 1 "22pF" H 2725 4400 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2738 4350 50  0001 C CNN
F 3 "" H 2700 4500 50  0000 C CNN
	1    2700 4500
	1    0    0    -1  
$EndComp
$Comp
L CP-RESCUE-pinguino_2550 C5
U 1 1 5767F3D4
P 8250 3700
F 0 "C5" H 8275 3800 50  0000 L CNN
F 1 "10uF" H 8275 3600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D6.3_L11.2_P2.5" H 8288 3550 50  0001 C CNN
F 3 "" H 8250 3700 50  0000 C CNN
	1    8250 3700
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-pinguino_2550 D1
U 1 1 5767F76B
P 2100 1350
F 0 "D1" H 2100 1450 50  0000 C CNN
F 1 "LED" H 2100 1250 50  0000 C CNN
F 2 "LEDs:LED-5MM" H 2100 1350 50  0001 C CNN
F 3 "" H 2100 1350 50  0000 C CNN
	1    2100 1350
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR01
U 1 1 5767FA5E
P 8250 3300
F 0 "#PWR01" H 8250 3150 50  0001 C CNN
F 1 "+5V" H 8250 3440 50  0000 C CNN
F 2 "" H 8250 3300 50  0000 C CNN
F 3 "" H 8250 3300 50  0000 C CNN
	1    8250 3300
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-pinguino_2550 #PWR02
U 1 1 5767FAA8
P 8250 4250
F 0 "#PWR02" H 8250 4000 50  0001 C CNN
F 1 "GND" H 8250 4100 50  0000 C CNN
F 2 "" H 8250 4250 50  0000 C CNN
F 3 "" H 8250 4250 50  0000 C CNN
	1    8250 4250
	-1   0    0    -1  
$EndComp
$Comp
L GND-RESCUE-pinguino_2550 #PWR03
U 1 1 5768012A
P 2450 4650
F 0 "#PWR03" H 2450 4400 50  0001 C CNN
F 1 "GND" H 2450 4500 50  0000 C CNN
F 2 "" H 2450 4650 50  0000 C CNN
F 3 "" H 2450 4650 50  0000 C CNN
	1    2450 4650
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW1
U 1 1 576801E5
P 2500 2000
F 0 "SW1" H 2650 2110 50  0000 C CNN
F 1 "Reset" H 2500 1920 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_DIP_x1_Slide" H 2500 2000 50  0001 C CNN
F 3 "" H 2500 2000 50  0000 C CNN
	1    2500 2000
	0    1    1    0   
$EndComp
$Comp
L GND-RESCUE-pinguino_2550 #PWR04
U 1 1 576803FF
P 2500 2400
F 0 "#PWR04" H 2500 2150 50  0001 C CNN
F 1 "GND" H 2500 2250 50  0000 C CNN
F 2 "" H 2500 2400 50  0000 C CNN
F 3 "" H 2500 2400 50  0000 C CNN
	1    2500 2400
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 576804D7
P 2500 1050
F 0 "#PWR05" H 2500 900 50  0001 C CNN
F 1 "+5V" H 2500 1190 50  0000 C CNN
F 2 "" H 2500 1050 50  0000 C CNN
F 3 "" H 2500 1050 50  0000 C CNN
	1    2500 1050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR06
U 1 1 57680862
P 2100 1050
F 0 "#PWR06" H 2100 900 50  0001 C CNN
F 1 "+5V" H 2100 1190 50  0000 C CNN
F 2 "" H 2100 1050 50  0000 C CNN
F 3 "" H 2100 1050 50  0000 C CNN
	1    2100 1050
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-pinguino_2550 #PWR07
U 1 1 576808C8
P 2100 2400
F 0 "#PWR07" H 2100 2150 50  0001 C CNN
F 1 "GND" H 2100 2250 50  0000 C CNN
F 2 "" H 2100 2400 50  0000 C CNN
F 3 "" H 2100 2400 50  0000 C CNN
	1    2100 2400
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-pinguino_2550 #PWR08
U 1 1 57680BD6
P 8500 2450
F 0 "#PWR08" H 8500 2200 50  0001 C CNN
F 1 "GND" H 8500 2300 50  0000 C CNN
F 2 "" H 8500 2450 50  0000 C CNN
F 3 "" H 8500 2450 50  0000 C CNN
	1    8500 2450
	-1   0    0    -1  
$EndComp
$Comp
L GND-RESCUE-pinguino_2550 #PWR09
U 1 1 576810CC
P 3550 3500
F 0 "#PWR09" H 3550 3250 50  0001 C CNN
F 1 "GND" H 3550 3350 50  0000 C CNN
F 2 "" H 3550 3500 50  0000 C CNN
F 3 "" H 3550 3500 50  0000 C CNN
	1    3550 3500
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-pinguino_2550 D3
U 1 1 576815B9
P 6650 1200
F 0 "D3" H 6650 1300 50  0000 C CNN
F 1 "LED" H 6650 1100 50  0000 C CNN
F 2 "LEDs:LED-5MM" H 6650 1200 50  0001 C CNN
F 3 "" H 6650 1200 50  0000 C CNN
	1    6650 1200
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR010
U 1 1 576816BF
P 6650 900
F 0 "#PWR010" H 6650 750 50  0001 C CNN
F 1 "+5V" H 6650 1040 50  0000 C CNN
F 2 "" H 6650 900 50  0000 C CNN
F 3 "" H 6650 900 50  0000 C CNN
	1    6650 900 
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-pinguino_2550 #PWR011
U 1 1 57681902
P 3200 3100
F 0 "#PWR011" H 3200 2850 50  0001 C CNN
F 1 "GND" H 3200 2950 50  0000 C CNN
F 2 "" H 3200 3100 50  0000 C CNN
F 3 "" H 3200 3100 50  0000 C CNN
	1    3200 3100
	1    0    0    -1  
$EndComp
Text GLabel 5950 4250 3    60   Input ~ 0
Rb7
Text GLabel 6100 4250 3    60   Input ~ 0
Rb6
Text GLabel 6250 4250 3    60   Input ~ 0
Rb5
Text GLabel 6400 4250 3    60   Input ~ 0
Rb4
Text GLabel 6500 3400 1    60   Input ~ 0
Rb3
Text GLabel 6350 3400 1    60   Input ~ 0
Rb2
Text GLabel 6200 3400 1    60   Input ~ 0
Rb1
Text GLabel 6050 3400 1    60   Input ~ 0
Rb0
Text GLabel 6550 2850 0    60   Input ~ 0
Rc7
Text GLabel 6550 3000 0    60   Input ~ 0
Rc6
$Comp
L CRYSTAL X1
U 1 1 57693CFD
P 3150 4400
F 0 "X1" H 3150 4550 60  0000 C CNN
F 1 "20MHz" H 3150 4250 60  0000 C CNN
F 2 "Crystals:HC-49V" H 3150 4400 60  0001 C CNN
F 3 "" H 3150 4400 60  0000 C CNN
	1    3150 4400
	0    1    1    0   
$EndComp
$Comp
L USB_1 J1
U 1 1 57695B64
P 8150 1750
F 0 "J1" H 8000 2150 60  0000 C CNN
F 1 "USB_1" H 8125 1150 60  0001 C CNN
F 2 "Connect:USB_B" H 8150 1750 60  0001 C CNN
F 3 "" H 8150 1750 60  0000 C CNN
	1    8150 1750
	1    0    0    -1  
$EndComp
Text GLabel 7500 1950 0    60   Input ~ 0
Vusb
Text GLabel 2600 2750 0    60   Input ~ 0
Vusb
Text GLabel 8750 1950 2    60   Input ~ 0
D+
Text GLabel 8750 2100 2    60   Input ~ 0
D-
Text GLabel 6100 3000 2    60   Input ~ 0
D+
Text GLabel 6100 2850 2    60   Input ~ 0
D-
Text GLabel 6000 1650 1    60   Input ~ 0
Ra0
Text GLabel 6150 1650 1    60   Input ~ 0
Ra1
Text GLabel 6300 1650 1    60   Input ~ 0
Ra2
Text GLabel 6450 1650 1    60   Input ~ 0
Ra3
Text GLabel 6100 2250 2    60   Input ~ 0
Ra5
Text GLabel 6100 2400 2    60   Input ~ 0
Rc0
Text GLabel 6100 2550 2    60   Input ~ 0
Rc1
Text GLabel 6100 2700 2    60   Input ~ 0
Rc2
$Comp
L CONN_11 P1
U 1 1 5769917A
P 3750 6200
F 0 "P1" V 3700 6200 60  0000 C CNN
F 1 "CONN_11" V 3800 6200 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x11" H 3750 6200 60  0001 C CNN
F 3 "" H 3750 6200 60  0000 C CNN
	1    3750 6200
	0    1    1    0   
$EndComp
$Comp
L CONN_11 P2
U 1 1 57699335
P 5150 6200
F 0 "P2" V 5100 6200 60  0000 C CNN
F 1 "CONN_11" V 5200 6200 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x11" H 5150 6200 60  0001 C CNN
F 3 "" H 5150 6200 60  0000 C CNN
	1    5150 6200
	0    1    1    0   
$EndComp
$Comp
L GND-RESCUE-pinguino_2550 #PWR012
U 1 1 576993D8
P 4450 5950
F 0 "#PWR012" H 4450 5700 50  0001 C CNN
F 1 "GND" H 4450 5800 50  0000 C CNN
F 2 "" H 4450 5950 50  0000 C CNN
F 3 "" H 4450 5950 50  0000 C CNN
	1    4450 5950
	-1   0    0    -1  
$EndComp
Text GLabel 3250 5450 0    60   Input ~ 0
Ra0
Text GLabel 3250 5300 0    60   Input ~ 0
Ra1
Text GLabel 3250 5150 0    60   Input ~ 0
Ra2
Text GLabel 3250 5000 0    60   Input ~ 0
Ra3
Text GLabel 4250 5000 2    60   Input ~ 0
Ra5
Text GLabel 4250 5300 2    60   Input ~ 0
Rc1
Text GLabel 4250 5450 2    60   Input ~ 0
Rc2
Text GLabel 4250 5150 2    60   Input ~ 0
Rc0
Text GLabel 4750 5600 0    60   Input ~ 0
Rb7
Text GLabel 4750 5450 0    60   Input ~ 0
Rb6
Text GLabel 4750 5300 0    60   Input ~ 0
Rb5
Text GLabel 4750 5150 0    60   Input ~ 0
Rb4
Text GLabel 4250 5600 2    60   Input ~ 0
Rc6
Text GLabel 5700 5450 2    60   Input ~ 0
Rc7
Text GLabel 4750 5000 0    60   Input ~ 0
Rb3
Text GLabel 5700 5000 2    60   Input ~ 0
Rb2
Text GLabel 5700 5150 2    60   Input ~ 0
Rb1
Text GLabel 5700 5300 2    60   Input ~ 0
Rb0
$Comp
L +5V #PWR013
U 1 1 57680B08
P 3300 2650
F 0 "#PWR013" H 3300 2500 50  0001 C CNN
F 1 "+5V" H 3300 2790 50  0000 C CNN
F 2 "" H 3300 2650 50  0000 C CNN
F 3 "" H 3300 2650 50  0000 C CNN
	1    3300 2650
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR014
U 1 1 576996F4
P 3250 5700
F 0 "#PWR014" H 3250 5550 50  0001 C CNN
F 1 "+5V" H 3250 5840 50  0000 C CNN
F 2 "" H 3250 5700 50  0000 C CNN
F 3 "" H 3250 5700 50  0000 C CNN
	1    3250 5700
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR015
U 1 1 5769961D
P 5650 5700
F 0 "#PWR015" H 5650 5550 50  0001 C CNN
F 1 "+5V" H 5650 5840 50  0000 C CNN
F 2 "" H 5650 5700 50  0000 C CNN
F 3 "" H 5650 5700 50  0000 C CNN
	1    5650 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1750 3500 1750
Wire Wire Line
	7700 2350 7700 2400
Wire Wire Line
	2500 1050 2500 1100
Wire Wire Line
	2500 1600 2500 1700
Wire Wire Line
	2500 2300 2500 2400
Wire Wire Line
	2100 2250 2100 2400
Wire Wire Line
	2900 1750 2900 1650
Wire Wire Line
	2900 1650 2500 1650
Connection ~ 2500 1650
Wire Wire Line
	2350 4100 3500 4100
Wire Wire Line
	2700 4700 3500 4700
Wire Wire Line
	2700 4300 2600 4300
Wire Wire Line
	2600 4300 2600 4450
Wire Wire Line
	2600 4450 2500 4450
Wire Wire Line
	2500 4450 2500 4550
Wire Wire Line
	2500 4550 2350 4550
Wire Wire Line
	2350 4550 2350 4500
Wire Wire Line
	2450 4550 2450 4650
Connection ~ 2450 4550
Connection ~ 3150 4100
Wire Wire Line
	3500 4700 3500 4200
Connection ~ 3150 4700
Wire Wire Line
	3550 3500 3550 3350
Wire Wire Line
	3550 3350 3250 3350
Wire Wire Line
	3250 3350 3250 3450
Wire Wire Line
	3500 3900 3250 3900
Wire Wire Line
	3250 3900 3250 3850
Wire Wire Line
	3500 2950 3200 2950
Wire Wire Line
	3200 2950 3200 3100
Wire Wire Line
	3500 3150 3300 3150
Wire Wire Line
	3300 3150 3300 2950
Connection ~ 3300 2950
Wire Wire Line
	3200 2750 3500 2750
Wire Wire Line
	7500 1950 7750 1950
Wire Wire Line
	6650 2150 6650 1950
Wire Wire Line
	5900 2150 6650 2150
Wire Wire Line
	8500 1950 8750 1950
Wire Wire Line
	8750 2100 8500 2100
Wire Wire Line
	6100 2850 5900 2850
Wire Wire Line
	6100 3000 6050 3000
Wire Wire Line
	6050 3000 6050 2950
Wire Wire Line
	6050 2950 5900 2950
Wire Wire Line
	8500 2200 8550 2200
Wire Wire Line
	8550 2200 8550 2400
Wire Wire Line
	8550 2400 8450 2400
Wire Wire Line
	8500 2400 8500 2450
Wire Wire Line
	7650 2300 8450 2300
Wire Wire Line
	8450 2300 8450 2400
Connection ~ 8500 2400
Wire Wire Line
	7750 2100 7650 2100
Wire Wire Line
	7650 2100 7650 2300
Wire Wire Line
	7750 2200 7650 2200
Connection ~ 7650 2200
Wire Wire Line
	7850 3400 8650 3400
Wire Wire Line
	7850 3400 7850 3500
Wire Wire Line
	8250 3300 8250 3500
Wire Wire Line
	8250 3900 8250 4250
Wire Wire Line
	7850 4000 8650 4000
Wire Wire Line
	7850 4000 7850 3900
Wire Wire Line
	2100 1050 2100 1150
Wire Wire Line
	2100 1550 2100 1750
Wire Wire Line
	6650 1400 6650 1450
Wire Wire Line
	6650 1000 6650 900 
Wire Wire Line
	5900 4150 5950 4150
Wire Wire Line
	5950 4150 5950 4250
Wire Wire Line
	6100 4250 6100 4050
Wire Wire Line
	6100 4050 5900 4050
Wire Wire Line
	5900 3950 6250 3950
Wire Wire Line
	6250 3950 6250 4250
Wire Wire Line
	6400 4250 6400 3850
Wire Wire Line
	6400 3850 5900 3850
Wire Wire Line
	5900 3450 6050 3450
Wire Wire Line
	6050 3450 6050 3400
Wire Wire Line
	6200 3400 6200 3550
Wire Wire Line
	6200 3550 5900 3550
Wire Wire Line
	6350 3400 6350 3650
Wire Wire Line
	6350 3650 5900 3650
Wire Wire Line
	6500 3400 6500 3750
Wire Wire Line
	6500 3750 5900 3750
Wire Wire Line
	6550 3000 6600 3000
Wire Wire Line
	6600 3000 6600 3100
Wire Wire Line
	6600 3100 6050 3100
Wire Wire Line
	6050 3100 6050 3050
Wire Wire Line
	6050 3050 5900 3050
Wire Wire Line
	6550 2850 6650 2850
Wire Wire Line
	6650 2850 6650 3150
Wire Wire Line
	6650 3150 5900 3150
Wire Wire Line
	5900 1750 6000 1750
Wire Wire Line
	6000 1750 6000 1650
Wire Wire Line
	6150 1650 6150 1850
Wire Wire Line
	6150 1850 5900 1850
Wire Wire Line
	6300 1650 6300 1950
Wire Wire Line
	6300 1950 5900 1950
Wire Wire Line
	6450 1650 6450 2050
Wire Wire Line
	6450 2050 5900 2050
Wire Wire Line
	6100 2250 5900 2250
Wire Wire Line
	5900 2550 5900 2400
Wire Wire Line
	5900 2400 6100 2400
Wire Wire Line
	6100 2700 6000 2700
Wire Wire Line
	6000 2700 6000 2750
Wire Wire Line
	6000 2750 5900 2750
Wire Wire Line
	4250 5850 4250 5700
Wire Wire Line
	4250 5700 4650 5700
Wire Wire Line
	4450 5700 4450 5950
Wire Wire Line
	4650 5700 4650 5850
Connection ~ 4450 5700
Wire Wire Line
	3250 5850 3250 5700
Wire Wire Line
	5650 5850 5650 5700
Wire Wire Line
	3350 5450 3350 5850
Wire Wire Line
	3450 5300 3450 5850
Wire Wire Line
	3250 5300 3450 5300
Wire Wire Line
	3550 5150 3550 5850
Wire Wire Line
	3250 5150 3550 5150
Wire Wire Line
	3250 5000 3650 5000
Wire Wire Line
	3650 5000 3650 5850
Wire Wire Line
	3350 5450 3250 5450
Wire Wire Line
	4250 5000 3750 5000
Wire Wire Line
	3750 5000 3750 5850
Wire Wire Line
	4250 5150 3850 5150
Wire Wire Line
	3850 5150 3850 5850
Wire Wire Line
	4250 5300 3950 5300
Wire Wire Line
	3950 5300 3950 5850
Wire Wire Line
	4250 5450 4050 5450
Wire Wire Line
	4050 5450 4050 5850
Wire Wire Line
	4250 5600 4150 5600
Wire Wire Line
	4150 5600 4150 5850
Wire Wire Line
	4800 5600 4750 5600
Wire Wire Line
	4800 5600 4800 5750
Wire Wire Line
	4800 5750 4750 5750
Wire Wire Line
	4750 5750 4750 5850
Wire Wire Line
	4850 5450 4850 5850
Wire Wire Line
	4950 5850 4950 5300
Wire Wire Line
	4850 5450 4750 5450
Wire Wire Line
	4950 5300 4750 5300
Wire Wire Line
	4750 5150 5050 5150
Wire Wire Line
	5050 5150 5050 5850
Wire Wire Line
	4750 5000 5150 5000
Wire Wire Line
	5150 5000 5150 5850
Wire Wire Line
	5700 5450 5550 5450
Wire Wire Line
	5550 5450 5550 5850
Wire Wire Line
	5450 5850 5450 5300
Wire Wire Line
	5450 5300 5700 5300
Wire Wire Line
	5700 5150 5350 5150
Wire Wire Line
	5350 5150 5350 5850
Wire Wire Line
	5250 5000 5250 5850
Wire Wire Line
	5250 5000 5700 5000
Wire Wire Line
	5900 2650 5950 2650
Wire Wire Line
	5950 2650 5950 2550
Wire Wire Line
	5950 2550 6100 2550
Connection ~ 3300 2750
Wire Wire Line
	3300 2650 3300 2750
Wire Wire Line
	2600 2750 2800 2750
$Comp
L DIODE-RESCUE-pinguino_2550 D2
U 1 1 5769576F
P 3000 2750
F 0 "D2" H 3000 2850 40  0000 C CNN
F 1 "DIODE" H 3000 2650 40  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 3000 2750 60  0001 C CNN
F 3 "" H 3000 2750 60  0000 C CNN
	1    3000 2750
	-1   0    0    1   
$EndComp
$Comp
L C-RESCUE-pinguino_2550 C6
U 1 1 5769EA7E
P 8650 3700
F 0 "C6" H 8700 3800 50  0000 L CNN
F 1 "220nF" H 8700 3600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 8650 3700 60  0001 C CNN
F 3 "" H 8650 3700 60  0000 C CNN
	1    8650 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3400 8650 3500
Connection ~ 8250 3400
Wire Wire Line
	8650 4000 8650 3900
Connection ~ 8250 4000
$EndSCHEMATC
